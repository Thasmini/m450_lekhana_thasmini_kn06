package ch.tbz.recipe.planner.mapper;

import ch.tbz.recipe.planner.domain.Ingredient;
import ch.tbz.recipe.planner.domain.Recipe;
import ch.tbz.recipe.planner.domain.Unit;
import ch.tbz.recipe.planner.entities.IngredientEntity;
import ch.tbz.recipe.planner.entities.RecipeEntity;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.UUID;

public class MapperTest {

    private final IngredientEntityMapper ingredientEntityMapper = new IngredientEntityMapperImpl();
    private final RecipeEntityMapper recipeEntityMapper = new RecipeEntityMapperImpl();

    @Test
    public void testIngredientMapping() {
        // Given
        IngredientEntity ingredientEntity = new IngredientEntity(UUID.randomUUID(), "Sugar", "Sweet", "GRAMM", 200);

        // When
        Ingredient ingredient = ingredientEntityMapper.entityToDomain(ingredientEntity);
        IngredientEntity mappedIngredientEntity = ingredientEntityMapper.domainToEntity(ingredient);

        // Then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(ingredient.getId()).isEqualTo(ingredientEntity.getId());
        softly.assertThat(ingredient.getName()).isEqualTo(ingredientEntity.getName());
        softly.assertThat(ingredient.getComment()).isEqualTo(ingredientEntity.getComment());
        softly.assertThat(ingredient.getUnit()).isEqualTo(ingredientEntity.getUnit());
        softly.assertThat(ingredient.getAmount()).isEqualTo(ingredientEntity.getAmount());

        softly.assertThat(mappedIngredientEntity.getId()).isEqualTo(ingredient.getId());
        softly.assertThat(mappedIngredientEntity.getName()).isEqualTo(ingredient.getName());
        softly.assertThat(mappedIngredientEntity.getComment()).isEqualTo(ingredient.getComment());
        softly.assertThat(mappedIngredientEntity.getUnit()).isEqualTo(ingredient.getUnit());
        softly.assertThat(mappedIngredientEntity.getAmount()).isEqualTo(ingredient.getAmount());

        softly.assertAll();
    }

    @Test
    public void testRecipeMapping() {
        // Given
        UUID ingredientId = UUID.randomUUID();
        IngredientEntity ingredientEntity = new IngredientEntity(ingredientId, "Flour", "For baking", "GRAMM", 500);
        Ingredient ingredient = new Ingredient(ingredientId, "Flour", "For baking", Unit.GRAMM, 500);
        RecipeEntity recipeEntity = new RecipeEntity(UUID.randomUUID(), "Cake", "Delicious cake", "cake.jpg", Arrays.asList(ingredientEntity));
        Recipe recipe = new Recipe(recipeEntity.getId(), "Cake", "Delicious cake", "cake.jpg", Arrays.asList(ingredient));

        // When
        Recipe mappedRecipe = recipeEntityMapper.entityToDomain(recipeEntity);
        RecipeEntity mappedRecipeEntity = recipeEntityMapper.domainToEntity(recipe);

        // Then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(mappedRecipe.getId()).isEqualTo(recipe.getId());
        softly.assertThat(mappedRecipe.getName()).isEqualTo(recipe.getName());
        softly.assertThat(mappedRecipe.getDescription()).isEqualTo(recipe.getDescription());
        softly.assertThat(mappedRecipe.getImageUrl()).isEqualTo(recipe.getImageUrl());

        softly.assertThat(mappedRecipeEntity.getId()).isEqualTo(recipeEntity.getId());
        softly.assertThat(mappedRecipeEntity.getName()).isEqualTo(recipe.getName());
        softly.assertThat(mappedRecipeEntity.getDescription()).isEqualTo(recipe.getDescription());
        softly.assertThat(mappedRecipeEntity.getImageUrl()).isEqualTo(recipe.getImageUrl());

        softly.assertAll();
    }
}
